<?php

require_once './vendor/autoload.php';

use Matok\Chart\ChartCanvas;

echo 'Open Closed Principle'."\n\n";

$canvas = new ChartCanvas();

$canvas->addLine(1, 3, 10, 7);
$canvas->addPoint(5, 5);
$canvas->addBar(5, 12);

$canvas->draw();