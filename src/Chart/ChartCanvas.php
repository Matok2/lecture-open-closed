<?php

namespace Matok\Chart;

use Matok\Chart\Type\Bar;
use Matok\Chart\Type\Line;
use Matok\Chart\Type\Point;

class ChartCanvas
{
    private $items;

    public function addLine($x1, $y1, $x2, $y2)
    {
        $line = new Line($x1, $y1, $x2, $y2);
        $this->items[] = $line;
    }

    public function addPoint($xAxis, $yAxis)
    {
        $point = new Point($xAxis, $yAxis);
        $this->items[] = $point;

    }

    public function addBar($xAxis, $height)
    {
        $bar = new Bar($xAxis, $height);
        $this->items[] = $bar;
    }

    public function draw()
    {
        echo '..:: Drawing Chart Items ::..'."\n\n";

        $idx = 0;
        foreach ($this->items as $chartItem) {
            echo '# '.(++$idx)."\n";
            switch ($chartItem->getType()) {
                case 'line':
                    echo '  ... drawing [LINE]: ';
                    echo '['.$chartItem->getX1().' , '.$chartItem->getY1().'] ... ['.$chartItem->getX2().', '.$chartItem->getY2().']';
                    break;
                case 'point':
                    echo '  ... drawing [POINT]: ';
                    echo '['.$chartItem->getXAxis().', '.$chartItem->getYAxis().']';
                    break;
                case 'bar':
                    echo '  ... drawing [BAR]: ';
                    echo 'X: '.$chartItem->getXAxis().' height: '.$chartItem->getHeight();
                    break;
            }
            echo "\n";
        }
    }

    public function getMaxXAxisPoint()
    {
        foreach ($this->items as $chartItem) {
            switch ($chartItem->getType()) {
                case 'line':
                    break;
                case 'point':
                    break;
                default:
                    break;
            }
        }
    }

}