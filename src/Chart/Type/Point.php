<?php

namespace Matok\Chart\Type;

class Point implements ChartTypeInterface, StupidTypeInterface
{
    private $xAxis;
    private $yAxis;

    public function __construct($x, $y)
    {
        $this->xAxis = $x;
        $this->yAxis = $y;
    }

    public function getXAxis()
    {
        return $this->xAxis;
    }

    public function getYAxis()
    {
        return $this->yAxis;
    }

    public function getType()
    {
        return 'point';
    }
}