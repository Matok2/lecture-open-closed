<?php

namespace Matok\Chart\Type;

class Bar implements ChartTypeInterface, StupidTypeInterface
{
    private $xAxis;
    private $height;
    private $thickness;

    public function __construct($xAxis, $height, $thickness = 10)
    {
        $this->xAxis = $xAxis;
        $this->height = $height;
        $this->thickness = $thickness;
    }

    public function getXAxis()
    {
        return $this->xAxis;
    }

    public function getHeight()
    {
        return $this->height;
    }

    public function getThickness()
    {
        return $this->thickness;
    }

    public function getType()
    {
        return 'bar';
    }
}