<?php

class Circle
{
    /** @var array */
    public $center;

    public $radius;

    public function __construct($x, $y, $radius)
    {
         $this->center = array($x, $y);
         $this->radius = $radius;
    }
}