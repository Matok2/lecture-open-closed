<?php

class Circle
{
    /** @var Point */
    public $center;

    public $radius;

    public function __construct(Point $center, $radius)
    {
         $this->center = $center;
         $this->radius = $radius;
    }
}

/*
BEFORE:
$myNiceCircle->center[0]

AFTER:

$myNiceCircle->center->getX()
*/