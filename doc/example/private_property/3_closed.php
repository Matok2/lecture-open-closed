<?php

class Circle
{
    /** @var Point */
    private $center;

    private $radius;

    public function __construct(Point $center, $radius)
    {
         $this->center = $center;
         $this->radius = $radius;
    }

    public function getCenter()
    {
        return array($this->center->getX(), $this->center->getY());
    }

    public function getCenterPoint()
    {
        return $this->center;
    }
}