<?php

class ClassicFactory
{
    public function createShape($type)
    {
        switch ($type) {
            case 'rectangle':
                return new Rectangle();

            case 'circle':
                return new Circle();
        }
    }
}