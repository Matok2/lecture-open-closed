<?php

class OcpFactoryBadWay
{
    private $shapeMap;

    public function registerShape($type, $callback)
    {
        $this->shapeMap[$type] = $callback;
    }

    public function createShape($type)
    {
        if (isset($this->shapeMap[$type])) {
            return $this->shapeMap[$type];
        }
    }
}